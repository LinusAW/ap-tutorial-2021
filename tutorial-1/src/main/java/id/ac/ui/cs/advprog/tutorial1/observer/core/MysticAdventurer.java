package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    @Override
    public void update() {
        Quest guildQuest = this.guild.getQuest();
        System.out.println(this.name + " has received quest");
        this.addQuest(guildQuest);
    }
}
