package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        Quest guildQuest = this.guild.getQuest();
        System.out.println(this.name + " has received quest");
        this.addQuest(guildQuest);
    }
}
