## TODO

- [X] Atur `application.properties` sesuai dengan konfigurasi database di local anda.
- [X] Baca percakapan dan buatlah `notes.md` terhadap requirement apa yang anda temukan
- [X] Buat model untuk sisa entity yang belum dibuat (Log dan tabel-tabel relationship antar table yang berhubungan) (
  jumlah relasi tergantung desain anda, dapat bervariasi)
  - [X] Asdos
  - [X] Log
  - [X] Review
- [X] Buat repository untuk entity yang belum dibuat. Anda dapat menambah query method pada repository yang sudah ada
  apabila dibutuhkan.
  - [X] Asdos
  - [X] Log
  - [X] Review
- [X] Buat Service untuk requirement yang anda temukan (2).
  - [X] Asdos
  - [X] Log
  - [X] Review
- [X] Buat RestController untuk requirement yang anda temukan (2)
  - [X] Asdos
  - [X] Log
  - [X] Review
- [ ] Uji coba aplikasi dengan client seperti PostMan.
- [ ] Tambahkan Badge Coverage dan Pipeline pada README ini.
- [X] Buat Test untuk setiap program yang anda ubah/tambahkan. Coverage aplikasi adalah 100% dalam keadaan sekarang.
  Mengurangi code coverage akan mengurangi nilai anda.
  - [X] Asdos
  - [X] Log
  - [X] Review

# Notes

## Part 1

Faculty and Students (MS) register for Teaching Assistant (Job Openings).

Many Students to OaOO (One and Only One) Course :: N Student -> 1 Course.

Students directly accepted to role.

TA foreignKeys:

- Course, protected.
- Student, protected.

```json
[
  {
    "Month": "January",
    "jamKerja": 10,
    "Pembayaran": 3500
  },
  {
    "Month": "February",
    "jamKerja": 40,
    "Pembayaran": 14000
  }
]
```

Logs:

- TA work: Reported to Faculty, updatable, API for payment reports.
- Payment: Viewed per month logs, API returns work logs and payment.

Log properties:

- Month
- Hours worked (in hours)
- Payment (in Greil, 350/hour)

Relationships defined in Spring Boot

```
Mahasiswa

NPM: varchar (Primary Key)
Nama : varchar
Email: varchar
ipk: varchar
noTelp: varchar

MataKuliah

KodeMatkul: varchar (Primary Key)
namaMatkul: varchar
prodi: varchar

Log

idLog: int (Primary Key, auto increment)
start: datetime
end: datetime
Deskripsi: varchar
```

## Part 2: REST API

## Testing method

Postman vs Curl