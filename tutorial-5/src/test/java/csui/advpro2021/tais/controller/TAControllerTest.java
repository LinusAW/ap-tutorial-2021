package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.TALog;
import csui.advpro2021.tais.model.TeachingAssistant;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import csui.advpro2021.tais.service.TeachingAssistantServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = TAController.class)
public class TAControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeachingAssistantServiceImpl taService;

    @MockBean
    private MataKuliahServiceImpl mkService;

    private MataKuliah matKul;
    private TeachingAssistant teachingAssistant;

    @BeforeEach
    public void setUp() {
        matKul = new MataKuliah();
        matKul.setKodeMatkul("0");
        matKul.setNama("ProgLan");
        matKul.setProdi("IlKom");

        teachingAssistant = new TeachingAssistant();
        teachingAssistant.setNpm("1906398761");
        teachingAssistant.setName("Linus");
        teachingAssistant.setEmail("linus.abhyasa@ui.ac.id");
        teachingAssistant.setPhone("0895");
        teachingAssistant.setCourse(matKul);
        taService.createTA(teachingAssistant);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListTA() throws Exception {
        List<TeachingAssistant> listTA = Arrays.asList(teachingAssistant);
        when(taService.getListTA()).thenReturn(listTA);

        mvc.perform(get("/Assistant").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].npm").value(teachingAssistant.getNpm()));
    }

    @Test
    void testControllerCreateTA() throws Exception {
        System.out.println("in test " + teachingAssistant);
        when(taService.createTA(teachingAssistant)).thenReturn(teachingAssistant);
        System.out.println("out test " + taService.createTA(teachingAssistant));

        mvc.perform(post("/Assistant")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(teachingAssistant))).andExpect(status().isOk())
                .andExpect(jsonPath("$.npm").value(teachingAssistant.getNpm()));
    }

    @Test
    void testControllerGetTA() throws Exception {
        when(taService.getTAByNPM(teachingAssistant.getNpm())).thenReturn(teachingAssistant);
        mvc.perform(get("/Assistant/" + teachingAssistant.getNpm()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(teachingAssistant.getName()));
    }

//    @Test
//    void testControllerGetListLogsByMonth() throws Exception {
//        when(taService.getLogsByMonth(3)).thenReturn(Arrays.asList(teachingAssistant));
//        mvc.perform(get("/logs/monthly/" + teachingAssistant.getMonth()).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[0].id").value(teachingAssistant.getID()));
//    }

    @Test
    void testControllerGetListTAByCourse() throws Exception {
        List<TeachingAssistant> listTA = Arrays.asList(teachingAssistant);
        when(taService.getTAByCourse(matKul)).thenReturn(listTA);
        when(mkService.getMataKuliah(matKul.getKodeMatkul())).thenReturn(matKul);
        mvc.perform(get("/Assistant/course/" + matKul.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].npm").value(teachingAssistant.getNpm()));
    }

//    @Test
//    public void testControllerGetNonExistLogs() throws Exception {
//        mvc.perform(get("/logs/BASDEAD").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
//    }

//    @Test
//    void testControllerUpdateMataKuliah() throws Exception {
//        logService.createMataKuliah(log);
//
//        String namaMatkul = "ADV125YIHA";
//        log.setNama(namaMatkul);
//
//        when(logService.updateMataKuliah(log.getKodeMatkul(), log)).thenReturn(log);
//
//        mvc.perform(put("/logs/" + log.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON)
//                .content(mapToJson(log)))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.nama").value(namaMatkul));
//    }
//
//    @Test
//    void testControllerDeleteMataKuliah() throws Exception {
//        logService.createMataKuliah(log);
//        mvc.perform(delete("/logs/" + log.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNoContent());
//    }
}
