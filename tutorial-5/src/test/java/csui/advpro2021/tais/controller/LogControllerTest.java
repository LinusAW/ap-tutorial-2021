package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.TALog;
import csui.advpro2021.tais.model.TeachingAssistant;
import csui.advpro2021.tais.service.TALogServiceImpl;
import csui.advpro2021.tais.service.TeachingAssistantServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TALogServiceImpl logService;

    @MockBean
    private TeachingAssistantServiceImpl taService;

    private TALog taLog;
    private TeachingAssistant teachingAssistant;

    @BeforeEach
    public void setUp() {
        teachingAssistant = new TeachingAssistant();
        teachingAssistant.setNpm("1906398761");
        teachingAssistant.setName("Linus");
        teachingAssistant.setEmail("linus.abhyasa@ui.ac.id");
        teachingAssistant.setPhone("0895");
        taService.createTA(teachingAssistant);

        taLog = new TALog();
        taLog.setID(0);
        taLog.setMonth(3);
        taLog.setTimeStamp(Timestamp.valueOf(LocalDateTime.now())); //DatetimeNow
        taLog.setHoursWorked(4);
        taLog.setTeachingAssistant(teachingAssistant);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListLogs() throws Exception {
        List<TALog> listLog = Arrays.asList(taLog);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/logs").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(taLog.getID()));
    }

    @Test
    void testControllerCreateLog() throws Exception {
        System.out.println("in test " + taLog);
        when(logService.createLog(taLog)).thenReturn(taLog);
        System.out.println("out test " + logService.createLog(taLog));

        mvc.perform(post("/logs")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(taLog))).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(taLog.getID()));
    }

    @Test
    void testControllerGetLog() throws Exception {
        when(logService.getLogByID(taLog.getID())).thenReturn(taLog);
        mvc.perform(get("/logs/" + taLog.getID()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.month").value(taLog.getMonth()));
    }

    @Test
    void testControllerGetListLogsByMonth() throws Exception {
        when(logService.getLogsByMonth(3)).thenReturn(Arrays.asList(taLog));
        mvc.perform(get("/logs/monthly/" + taLog.getMonth()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(taLog.getID()));
    }

    @Test
    void testControllerGetListLogsByTA() throws Exception {
        List<TALog> listLog = Arrays.asList(taLog);
        when(taService.getTAByNPM(teachingAssistant.getNpm())).thenReturn(teachingAssistant);
        when(logService.getLogsByTA(teachingAssistant)).thenReturn(listLog);
        mvc.perform(get("/logs/TA/" + teachingAssistant.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(taLog.getID()));
    }

//    @Test
//    public void testControllerGetNonExistLogs() throws Exception {
//        mvc.perform(get("/logs/BASDEAD").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
//    }

//    @Test
//    void testControllerUpdateMataKuliah() throws Exception {
//        logService.createMataKuliah(log);
//
//        String namaMatkul = "ADV125YIHA";
//        log.setNama(namaMatkul);
//
//        when(logService.updateMataKuliah(log.getKodeMatkul(), log)).thenReturn(log);
//
//        mvc.perform(put("/logs/" + log.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON)
//                .content(mapToJson(log)))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.nama").value(namaMatkul));
//    }
//
//    @Test
//    void testControllerDeleteMataKuliah() throws Exception {
//        logService.createMataKuliah(log);
//        mvc.perform(delete("/logs/" + log.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNoContent());
//    }
}
