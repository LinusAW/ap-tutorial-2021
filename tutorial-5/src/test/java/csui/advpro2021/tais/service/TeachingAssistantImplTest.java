package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.TeachingAssistant;
import csui.advpro2021.tais.repository.TeachingAssistantRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.lenient;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class TeachingAssistantImplTest {
    @Mock
    private TeachingAssistantRepository teachingAssistantRepository;

    @InjectMocks
    private TeachingAssistantServiceImpl teachingAssistantService;

    private MataKuliah testCourse;
    private Mahasiswa testStudent1;
    private Mahasiswa testStudent2;
    private TeachingAssistant teachingAssistant1;
    private TeachingAssistant teachingAssistant2;


    @BeforeEach
    public void setUp() {
        testCourse = new MataKuliah();
        testCourse.setKodeMatkul("POK");

        testStudent1 = new Mahasiswa();
        testStudent1.setNpm("1906192052");

        testStudent2 = new Mahasiswa();
        testStudent2.setNpm("1906398761");

        teachingAssistant1 = new TeachingAssistant();
        teachingAssistant1.setNpm("1906192052");
        teachingAssistant1.setName("Maung Meong");
        teachingAssistant1.setEmail("maung@cs.ui.ac.id");
        teachingAssistant1.setCourse(testCourse);
        teachingAssistant1.setStudent(testStudent1);

        teachingAssistant2 = new TeachingAssistant();
        teachingAssistant2.setNpm("1906398761");
        teachingAssistant2.setName("Linus A");
        teachingAssistant2.setEmail("linus.abhyasa@ui.ac.id");
        teachingAssistant2.setCourse(testCourse);
        teachingAssistant2.setStudent(testStudent2);
    }

    @Test
    public void testServiceCreateTA() {
        lenient().when(teachingAssistantService.createTA(teachingAssistant1)).thenReturn(teachingAssistant1);
    }

    @Test
    public void testServiceGetListTA() {
        List<TeachingAssistant> listTA = teachingAssistantRepository.findAll();
        lenient().when(teachingAssistantService.getListTA()).thenReturn(listTA);
        List<TeachingAssistant> listTAResult = teachingAssistantService.getListTA();
//        System.out.println(listTAResult);
//        assertFalse(listTAResult.isEmpty());
        Assertions.assertIterableEquals(listTA, listTAResult);
    }

    @Test
    public void testServiceGetListTAByCourse() {
        List<TeachingAssistant> listTA = teachingAssistantRepository.findByCourse(testCourse);
        lenient().when(teachingAssistantService.getTAByCourse(testCourse)).thenReturn(listTA);
        List<TeachingAssistant> listTAResult = teachingAssistantService.getTAByCourse(testCourse);
        Assertions.assertIterableEquals(listTA, listTAResult);
    }

    @Test
    public void testServiceGetTAByNpm() {
        lenient().when(teachingAssistantService.getTAByNPM("1906192052")).thenReturn(teachingAssistant1);
        TeachingAssistant resultTA = teachingAssistantService.getTAByNPM(teachingAssistant1.getNpm());
        assertEquals(teachingAssistant1.getNpm(), resultTA.getNpm());
        assertEquals(testStudent1.getNpm(), resultTA.getNpm());
    }

//    @Test
//    public void testServiceUpdateMahasiswa(){
//        teachingAssistantService.createTA(teachingAssistant1);
//        String currentIpkValue = teachingAssistant1.getIpk();
//        //Change IPK from 4 to 3
//        teachingAssistant1.setIpk("3");
//
//        lenient().when(teachingAssistantService.updateMahasiswa(teachingAssistant1.getNpm(), teachingAssistant1)).thenReturn(teachingAssistant1);
//        Mahasiswa resultMahasiswa = teachingAssistantService.updateMahasiswa(teachingAssistant1.getNpm(), teachingAssistant1);
//
//        assertNotEquals(resultMahasiswa.getIpk(), currentIpkValue);
//        assertEquals(resultMahasiswa.getNama(), teachingAssistant1.getNama());
//
//
//    }
}
