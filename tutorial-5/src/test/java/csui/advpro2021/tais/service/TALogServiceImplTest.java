package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.TALog;
import csui.advpro2021.tais.model.TeachingAssistant;
import csui.advpro2021.tais.repository.TALogRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class TALogServiceImplTest {
    @Mock
    private TALogRepository logRepository;

    @InjectMocks
    private TALogServiceImpl logService;

    private TALog log;
    private TALog log2;
    private TeachingAssistant testAssistant;

    @BeforeEach
    public void setUp() {
        testAssistant = new TeachingAssistant();
        testAssistant.setNpm("1906398761");
        testAssistant.setName("Linus");
        testAssistant.setEmail("linus.abhyasa@ui.ac.id");
        testAssistant.setPhone("0895");

        log = new TALog();
        log.setID(0);
        log.setMonth(3);
        log.setTimeStamp(Timestamp.valueOf(LocalDateTime.now())); //DatetimeNow
        log.setHoursWorked(4);
        log.setTeachingAssistant(testAssistant);
        logService.calculatePayment(log);
        logService.createLog(log);
//        System.out.println("created " + log);

        log2 = new TALog(Timestamp.valueOf(LocalDateTime.now()), 3, 5, null);
        logService.calculatePayment(log2);
        logService.createLog(log2);
    }

    @Test
    public void testServiceGetListLog() {
        List<TALog> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        List<TALog> listLogResult = logService.getListLog();
//        System.out.println(listLogResult);
//        assertFalse(listLogResult.isEmpty());
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceGetLogByID() {
        lenient().when(logService.getLogByID(0)).thenReturn(log);
        TALog resultLog = logService.getLogByID(log.getID());
        assertNotEquals(resultLog, null);
        assertEquals(log.getID(), resultLog.getID());
    }

    @Test
    public void testServiceGetLogByMonth() {
        lenient().when(logService.getLogByID(0)).thenReturn(log);

        List<TALog> listLogResult = logService.getLogsByMonth(3);
        Assertions.assertIterableEquals(listLogResult, logRepository.findAllByMonth(3));
    }

    @Test
    public void testServiceGetLogByTA() {
        lenient().when(logService.getLogByID(0)).thenReturn(log);
        logService.createLog(log2);

        List<TALog> listLogResult = logService.getLogsByTA(testAssistant);
//        assertFalse(listLogResult.isEmpty()); //Find Out Cause of empty
        Assertions.assertIterableEquals(listLogResult, logRepository.findAllByTeachingAssistant(testAssistant));
    }

    @Test
    public void testCalculatePayment() {
        Integer hours = log.getHoursWorked();
        Integer expectedPayment = hours * 350;
        assertEquals(log.getPayment(), expectedPayment);
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(log);
        Integer currentHoursWorked = log.getHoursWorked();
        Integer currentPayment = log.getPayment();
        //Change HoursWorked from 4 to 3
        log.setHoursWorked(3);
        logService.calculatePayment(log);

        lenient().when(logService.updateLog(log)).thenReturn(log);
        TALog resultLog = logService.updateLog(log);

        assertNotEquals(resultLog.getHoursWorked(), currentHoursWorked);
        assertNotEquals(resultLog.getPayment(), currentPayment);
        assertEquals(resultLog.getID(), log.getID());
    }

}
