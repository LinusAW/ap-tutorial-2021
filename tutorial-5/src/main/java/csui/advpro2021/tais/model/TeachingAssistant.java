package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "TeachingAssistant")
@Data
@NoArgsConstructor
public class TeachingAssistant {
    @Id
    @Column(name = "TA_npm", updatable = false, nullable = false)
    private String npm;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "TA_student", referencedColumnName = "npm")
    private Mahasiswa student;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "TA_course_id", referencedColumnName = "kode_matkul")
    private MataKuliah course;

    @Column(name = "TA_name")
    private String name;

    @Column(name = "TA_email")
    private String email;

    @Column(name = "TA_phone")
    private String phone;

    public TeachingAssistant(String npm, String name, String email, String phone, MataKuliah course, Mahasiswa student) {
        this.npm = npm;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.course = course;
        this.student = student;
    }

}
