package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "TALog")
@Data
@NoArgsConstructor
public class TALog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "log_ID")
    private int ID;

    @Column(name = "log_month", updatable = false, nullable = false)
    private int month;

    @Column(name = "log_timeStamp", updatable = false, nullable = false)
    private Timestamp timeStamp;

    @Column(name = "log_hoursWorked")
    private Integer hoursWorked;

    @Column(name = "log_payment")
    private Integer payment;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "log_TA", referencedColumnName = "TA_npm")
    private TeachingAssistant teachingAssistant;

    public TALog(Timestamp timeStamp, int month, Integer hoursWorked, TeachingAssistant teachingAssistant) {
        this.timeStamp = timeStamp;
        this.month = month;
        this.hoursWorked = hoursWorked;
        this.teachingAssistant = teachingAssistant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TALog log = (TALog) o;
        return ID == log.ID && month == log.month && Objects.equals(hoursWorked, log.hoursWorked) && Objects.equals(payment, log.payment) && Objects.equals(teachingAssistant, log.teachingAssistant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, month, hoursWorked, payment, teachingAssistant);
    }
}
