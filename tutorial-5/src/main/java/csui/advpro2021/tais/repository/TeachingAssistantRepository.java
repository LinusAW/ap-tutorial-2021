package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.TeachingAssistant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeachingAssistantRepository extends JpaRepository<TeachingAssistant, String> {
    TeachingAssistant findByNpm(String npm);

    List<TeachingAssistant> findByCourse(MataKuliah course);
}
