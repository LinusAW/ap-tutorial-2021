package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.TALog;
import csui.advpro2021.tais.model.TeachingAssistant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TALogRepository extends JpaRepository<TALog, String> {
    TALog findByID(int ID);

    List<TALog> findAllByMonth(int month);

    List<TALog> findAllByTeachingAssistant(TeachingAssistant TA);
}
