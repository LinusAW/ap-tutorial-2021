package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.TALog;
import csui.advpro2021.tais.model.TeachingAssistant;

import java.util.List;

public interface TALogService {
    TALog createLog(TALog log);

    List<TALog> getListLog();

    TALog getLogByID(int ID);

    List<TALog> getLogsByMonth(int month);

    List<TALog> getLogsByTA(TeachingAssistant TA);

    void calculatePayment(TALog log);

    TALog updateLog(TALog log);
}
