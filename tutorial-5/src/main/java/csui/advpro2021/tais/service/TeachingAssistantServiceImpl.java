package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.TeachingAssistant;
import csui.advpro2021.tais.repository.TeachingAssistantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeachingAssistantServiceImpl implements TeachingAssistantService {

    @Autowired
    private TeachingAssistantRepository teachingAssistantRepository;

    @Override
    public TeachingAssistant createTA(TeachingAssistant teachingAssistant) {
        teachingAssistantRepository.save(teachingAssistant);
        return teachingAssistant;
    }

    @Override
    public List<TeachingAssistant> getListTA() {
        return teachingAssistantRepository.findAll();
    }

    @Override
    public TeachingAssistant getTAByNPM(String npm) {
        return teachingAssistantRepository.findByNpm(npm);
    }

    @Override
    public List<TeachingAssistant> getTAByCourse(MataKuliah course) {
        List<TeachingAssistant> allTA = getListTA();
        List<TeachingAssistant> results = new ArrayList<>();

        for (TeachingAssistant TA : allTA) {
            if (TA.getCourse() == course) {
                results.add(TA);
            }
        }

        return results;
    }

//    @Override
//    public TeachingAssistant updateTA(String npm, TeachingAssistant teachingAssistant) {
//        teachingAssistant.setNpm(npm);
//        teachingAssistantRepository.save(teachingAssistant);
//        return teachingAssistant;
//    }
}
