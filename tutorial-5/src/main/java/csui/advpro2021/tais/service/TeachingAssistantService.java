package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.TeachingAssistant;

import java.util.List;

public interface TeachingAssistantService {
    TeachingAssistant createTA(TeachingAssistant teachingAssistant);

    List<TeachingAssistant> getListTA();

    TeachingAssistant getTAByNPM(String npm);

    List<TeachingAssistant> getTAByCourse(MataKuliah course);

//    TeachingAssistant updateTA(String npm, TeachingAssistant teachingAssistant);
}
