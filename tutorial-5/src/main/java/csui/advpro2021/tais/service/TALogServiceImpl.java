package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.TALog;
import csui.advpro2021.tais.model.TeachingAssistant;
import csui.advpro2021.tais.repository.TALogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class TALogServiceImpl implements TALogService {
    @Autowired
    private TALogRepository logRepository;

    @Override
    public TALog createLog(TALog taLog) {
        logRepository.save(taLog);
        log.info("create log in TALogServiceImpl " + taLog);
        return taLog;
    }

    @Override
    public List<TALog> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public TALog getLogByID(int ID) {
        return logRepository.findByID(ID);
    }

    @Override
    public List<TALog> getLogsByMonth(int month) {
        return logRepository.findAllByMonth(month);
    }

    @Override
    public List<TALog> getLogsByTA(TeachingAssistant TA) {
//        List<TALog> allLogs = logRepository.findAll();
//        List<TALog> logsByTA = new ArrayList<>();
//
//        for (TALog log : allLogs) {
//            if (log.getTeachingAssistant().getNpm() == TA_NPM) {
//                logsByTA.add(log);
//            }
//        }

        return logRepository.findAllByTeachingAssistant(TA);
    }

    @Override
    public void calculatePayment(TALog log) {
        Integer hours = log.getHoursWorked();
        Integer payment = hours * 350;
        log.setPayment(payment);
    }

    @Override
    public TALog updateLog(TALog log) {
        logRepository.save(log);
        return log;
    }
}


