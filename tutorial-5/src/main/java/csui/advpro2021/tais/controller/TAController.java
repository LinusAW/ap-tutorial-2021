package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.TeachingAssistant;
import csui.advpro2021.tais.service.MataKuliahService;
import csui.advpro2021.tais.service.TeachingAssistantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/Assistant")
public class TAController {
    @Autowired
    private TeachingAssistantService teachingAssistantService;

    @Autowired
    private MataKuliahService courseService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postTA(@RequestBody TeachingAssistant teachingAssistant) {
        return ResponseEntity.ok(teachingAssistantService.createTA(teachingAssistant));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TeachingAssistant>> getListTA() {
        return ResponseEntity.ok(teachingAssistantService.getListTA());
    }

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getTA(@PathVariable(value = "npm") String npm) {
        TeachingAssistant teachingAssistant = teachingAssistantService.getTAByNPM(npm);
        if (teachingAssistant == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(teachingAssistant);
    }

    @GetMapping(path = "course/{course}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TeachingAssistant>> getTAByCourse(@PathVariable(value = "course") String courseCode) {
        MataKuliah course = courseService.getMataKuliah(courseCode);
        return ResponseEntity.ok(teachingAssistantService.getTAByCourse(course));
    }

//    @PutMapping(path = "/{npm}", produces = {"application/json"})
//    @ResponseBody
//    public ResponseEntity updateMahasiswa(@PathVariable(value = "npm") String npm, @RequestBody Mahasiswa mahasiswa) {
//        return ResponseEntity.ok(mahasiswaService.updateMahasiswa(npm, mahasiswa));
//    }
//
//    @DeleteMapping(path = "/{npm}", produces = {"application/json"})
//    public ResponseEntity deleteMahasiswa(@PathVariable(value = "npm") String npm) {
//        mahasiswaService.deleteMahasiswaByNPM(npm);
//        return new ResponseEntity(HttpStatus.NO_CONTENT);
//    }
}
