package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.TALog;
import csui.advpro2021.tais.service.TALogService;
import csui.advpro2021.tais.service.TeachingAssistantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/logs")
@Slf4j
public class LogController {

    @Autowired
    private TALogService logService;

    @Autowired
    private TeachingAssistantService teachingAssistantService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@RequestBody TALog taLog) {
        log.info("post log " + taLog);
        TALog result = logService.createLog(taLog);
        log.info("result " + result);
        return ResponseEntity.ok(result);
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TALog>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(path = "/{ID}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "ID") int ID) {
        TALog log = logService.getLogByID(ID);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @GetMapping(path = "monthly/{month}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogsByMonth(@PathVariable(value = "month") int month) {
        return ResponseEntity.ok(logService.getLogsByMonth(month));
    }

    @GetMapping(path = "TA/{TA_NPM}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogsByTA(@PathVariable(value = "TA_NPM") String npm) {
        return ResponseEntity.ok(logService.getLogsByTA(teachingAssistantService.getTAByNPM(npm)));
    }

//    @PutMapping(path = "/{npm}", produces = {"application/json"})
//    @ResponseBody
//    public ResponseEntity updateMahasiswa(@PathVariable(value = "npm") String npm, @RequestBody TALog log) {
//        return ResponseEntity.ok(logService.updateMahasiswa(npm, mahasiswa));
//    }

//    @DeleteMapping(path = "/{npm}", produces = {"application/json"})
//    public ResponseEntity deleteMahasiswa(@PathVariable(value = "npm") String npm) {
//        logService.deleteMahasiswaByNPM(npm);
//        return new ResponseEntity(HttpStatus.NO_CONTENT);
//    }

}
